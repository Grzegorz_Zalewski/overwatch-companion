/**
 * Created by maksi on 27.05.2016.
 */
(function ($) {
    $(function () {
        var current = 1;
        var animation = '';
        var animation2 = '';

        var heros = [
            {id: 0, name: '', class: "", counters: [], against: [], difficulty: 0, info: ""},
            {
                id: 1,
                name: 'Genji',
                class: "offense",
                counters: [{id: 3, level: 1}, {id: 10, level: 3}, {id: 16, level: 1}, {id: 17, level: 1}, {id: 20, level: 3}],
                against: [],
                difficulty: 3,
                info: "Genji flings precise and deadly Shuriken at his targets, and uses his technologically-advanced katana to deflect projectiles or deliver a Swift Strike that cuts down enemies."
            },
            {
                id: 2,
                name: 'McCree',
                class: "offense",
                counters: [{id: 1, level: 3}, {id: 6, level: 2}, {id: 7, level: 2}, {id: 12, level: 2}],
                against: [],
                difficulty: 2,
                info: "Armed with his Peacekeeper revolver, McCree takes out targets with deadeye precision and dives out of danger with eagle-like speed."
            },
            {
                id: 3,
                name: 'Pharah',
                class: "offense",
                counters: [{id: 5, level: 3}, {id: 7, level: 2}, {id: 8, level: 2}, {id: 12, level: 2}, {id: 15, level: 2}],
                against: [],
                difficulty: 1,
                info: "Soaring through the air in her combat armor, and armed with a launcher that lays down high-explosive rockets, Pharah is a force to be reckoned with."
            },
            {
                id: 4,
                name: 'Reaper',
                class: "offense",
                counters: [{id: 2, level: 2}, {id: 3, level: 2}, {id: 9, level: 1}, {id: 18, level: 1}],
                against: [],
                difficulty: 1,
                info: "Hellfire Shotguns, the ghostly ability to become immune to damage, and the power to step between shadows make Reaper one of the deadliest beings on Earth."
            },
            {
                id: 5,
                name: 'Soldier-76',
                class: "offense",
                counters: [{id: 1, level: 2}, {id: 7, level: 3}, {id: 14, level: 2}],
                against: [],
                difficulty: 1,
                info: "Armed with cutting-edge weaponry, including an experimental pulse rifle that’s capable of firing spirals of high-powered Helix Rockets, Soldier: 76 has the speed and support know-how of a highly trained warrior."
            },
            {
                id: 6,
                name: 'Tracer',
                class: "offense",
                counters: [{id: 5, level: 2}, {id: 10, level: 2}, {id: 11, level: 1}, {id: 15, level: 2}],
                against: [],
                difficulty: 2,
                info: "Toting twin pulse pistols, energy-based time bombs, and rapid-fire banter, Tracer is able to \"blink\" through space and rewind her personal timeline as she battles to right wrongs the world over."
            },
            {
                id: 7,
                name: 'Bastion',
                class: "defense",
                counters: [{id: 1, level: 3}, {id: 3, level: 3}, {id: 4, level: 3}, {id: 6, level: 2}, {id: 8, level: 3}, {id: 12, level: 3}, {
                    id: 15,
                    level: 2
                }],
                against: [],
                difficulty: 1,
                info: "Repair protocols and the ability to transform between stationary Assault, mobile Recon and devastating Tank configurations provide Bastion with a high probability of victory."
            },
            {
                id: 8,
                name: 'Hanzo',
                class: "defense",
                counters: [{id: 1, level: 2}, {id: 4, level: 2}, {id: 6, level: 2}, {id: 12, level: 3}, {id: 13, level: 2}, {id: 16, level: 3}],
                against: [],
                difficulty: 3,
                info: "Hanzo’s versatile arrows can reveal his enemies or fragment to strike multiple targets. He can scale walls to fire his bow from on high, or summon a titanic spirit dragon."
            },
            {
                id: 9,
                name: 'Junkrat',
                class: "defense",
                counters: [{id: 2, level: 2}, {id: 3, level: 3}, {id: 4, level: 2}, {id: 1, level: 2}],
                against: [],
                difficulty: 2,
                info: "Junkrat’s area-denying armaments include a Frag Launcher that lobs bouncing grenades, Concussion Mines that send enemies flying, and Steel Traps that stop foes dead in their tracks."
            },
            {
                id: 10,
                name: 'Mei',
                class: "defense",
                counters: [{id: 2, level: 1}, {id: 3, level: 3}, {id: 4, level: 2}, {id: 9, level: 2}],
                against: [],
                difficulty: 3,
                info: "Mei’s weather-altering devices slow opponents and protect locations. Her Endothermic Blaster unleashes damaging icicles and frost streams, and she can Cryo-Freeze herself to guard against counterattacks, or obstruct the opposing team's movements with an Ice Wall."
            },
            {
                id: 11,
                name: 'Torbjörn',
                class: "defense",
                counters: [{id: 1, level: 3}, {id: 4, level: 2}, {id: 12, level: 2}, {id: 13, level: 3}, {id: 14, level: 1}],
                against: [],
                difficulty: 2,
                info: "Torbjörn’s extensive arsenal includes a rivet gun and hammer, as well as a personal forge that he can use to build upgradeable turrets and dole out protective armor packs."
            },
            {
                id: 12,
                name: 'Widowmaker',
                class: "defense",
                counters: [{id: 1, level: 1}, {id: 4, level: 1}, {id: 6, level: 2}, {id: 16, level: 3}],
                against: [],
                difficulty: 2,
                info: "Widowmaker equips herself with whatever it takes to eliminate her targets, including mines that dispense poisonous gas, a visor that grants her squad infra-sight, and a powerful sniper rifle that can fire in fully-automatic mode."
            },
            {
                id: 13,
                name: 'D.Va',
                class: "tank",
                counters: [{id: 2, level: 2}, {id: 10, level: 2}, {id: 21, level: 2}],
                against: [],
                difficulty: 2,
                info: "D.Va’s mech is nimble and powerful—its twin Fusion Cannons blast away with autofire at short range, and she can use its Boosters to barrel over enemies and obstacles, or deflect attacks with her projectile-dismantling Defense Matrix."
            },
            {
                id: 14,
                name: 'Reinhardt',
                class: "tank",
                counters: [{id: 2, level: 2}, {id: 10, level: 2}, {id: 21, level: 2}],
                against: [],
                difficulty: 1,
                info: "Clad in powered armor and swinging his hammer, Reinhardt leads a rocket-propelled charge across the battleground and defends his squadmates with a massive energy barrier."
            },
            {
                id: 15,
                name: 'Roadhog',
                class: "tank",
                counters: [{id: 2, level: 2}, {id: 13, level: 2}, {id: 21, level: 1}],
                against: [],
                difficulty: 1,
                info: "Roadhog uses his signature Chain Hook to pull his enemies close before shredding them with blasts from his Scrap Gun. He’s hardy enough to withstand tremendous damage, and can recover his health with a short breather."
            },
            {
                id: 16,
                name: 'Winston',
                class: "tank",
                counters: [{id: 2, level: 2}, {id: 1, level: 2}, {id: 7, level: 2}, {id: 21, level: 2}],
                against: [],
                difficulty: 2,
                info: "Winston wields impressive inventions—a jump pack, electricity-blasting Tesla Cannon, portable shield projector and more—with literal gorilla strength."
            },
            {
                id: 17,
                name: 'Zarya',
                class: "tank",
                counters: [{id: 2, level: 1}, {id: 4, level: 1}, {id: 7, level: 2}, {id: 21, level: 2}],
                against: [],
                difficulty: 3,
                info: "Deploying powerful personal barriers that convert incoming damage into energy for her massive Particle Cannon, Zarya is an invaluable asset on the front lines of any battle."
            },
            {
                id: 18,
                name: 'Lucio',
                class: "support",
                counters: [{id: 1, level: 2}, {id: 6, level: 2}, {id: 7, level: 2}, {id: 10, level: 2}],
                against: [],
                difficulty: 2,
                info: "On the battlefield, Lúcio’s cutting-edge Sonic Amplifier buffets enemies with projectiles and knocks foes back with blasts of sound. His songs can both heal his team or boost their movement speed, and he can switch between tracks on the fly."
            },
            {
                id: 19,
                name: 'Mercy',
                class: "support",
                counters: [{id: 1, level: 2}, {id: 5, level: 1}, {id: 6, level: 2}, {id: 7, level: 2}],
                against: [],
                difficulty: 1,
                info: "Mercy’s Valkyrie Suit helps keep her close to teammates like a guardian angel; healing, resurrecting or strengthening them with the beams emanating from her Caduceus Staff."
            },
            {
                id: 20,
                name: 'Symmetra',
                class: "support",
                counters: [{id: 3, level: 3}, {id: 9, level: 2}, {id: 15, level: 3}, {id: 16, level: 2}],
                against: [],
                difficulty: 2,
                info: "Symmetra utilizes her light-bending Photon Projector to dispatch adversaries, shield her associates, construct teleportation pads and deploy particle-blasting Sentry Turrets."
            },
            {
                id: 21,
                name: 'Zenyatta',
                class: "support",
                counters: [{id: 1, level: 2}, {id: 4, level: 2}, {id: 6, level: 2}, {id: 8, level: 2}, {id: 12, level: 3}],
                against: [],
                difficulty: 3,
                info: "Zenyatta calls upon orbs of harmony and discord to heal his teammates and weaken his opponents, all while pursuing a transcendent state of immunity to damage."
            }
        ];
        $(document).on('click', '#changeHero', function () {
            $('#loading').transition(
                {
                    animation: 'drop',
                    onComplete: function () {
                        $('#opponents').transition('drop');
                    }
                });
        });

        var changeHero = function (up) {
            if (up) {
                current++;
                animation = 'left';
                animation2 = 'right';
            } else {
                current--;
                animation = 'right';
                animation2 = 'left';
            }
            if (current == 22) {
                current = 1;
            } else {
                if (current == 0) {
                    current = 21
                }
            }
        };

        var render = function () {
            var model = heros[current];

            $('#hero').transition(
                {
                    animation: 'fade ' + animation,
                    duration: '200ms',
                    onComplete: function () {
                        $('#heroImage').attr('src', 'img/' + model.id + '.png');
                        $('#class').text(model.class);
                        $('#roleIcon').html('<span class="hero-detail-role-icon ' + model.class + '"></span>');
                        $('#name').text(model.name);
                        $('#description').text(model.info);
                    }
                }).transition({
                duration: '200ms',
                animation: 'fade ' + animation2
            });
            $('#opponents .column')
                .transition({
                    animation: 'fade ' + animation,
                    duration: '200ms'
                });


            $('#opponents').html("");
            for (var i = 0; i < model.counters.length; ++i) {
                var opponent = model.counters[i];
                $('#opponents').append('<div class="column"  style="display: none;"><div class="ui tiny image"><img src="img/' + opponent.id + '.png"><span class="rating"><i class="rating' + opponent.level + ' icon"></i></span></div></div>');
            }
            $('.rating3.icon')
                .transition('set looping')
                .transition('tada', '2000ms')
            ;
            $('#opponents .column')
                .transition({
                    animation: 'fade ' + animation2,
                    duration: '200ms'
                });

        };

        $(document).on('click', '#close', function () {
            overwolf.windows.getCurrentWindow(function (result) {
                if (result.status == "success") {
                    overwolf.windows.close(result.window.id);
                }
            });
        });

        var sq = {};
        sq.e = document.getElementById("hero");
        if (sq.e.addEventListener) {
            sq.e.addEventListener("mousewheel", MouseWheelHandler, false);
            sq.e.addEventListener("DOMMouseScroll", MouseWheelHandler, false);
        }
        else sq.e.attachEvent("onmousewheel", MouseWheelHandler);

        function MouseWheelHandler(e) {
            // cross-browser wheel delta
            var e = window.event || e;
            changeHero(e.wheelDelta == Math.abs(e.wheelDelta));
            render();
            return false;
        }


    });
})(jQuery);